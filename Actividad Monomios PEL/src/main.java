import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		System.out.println("A continuacion, por favor introduzca los coeficientes y exponentes"
				+ " deseados, para parar, escriba 0 en exponente");
		
		Scanner sc = new Scanner(System.in);
		List lista = new List();
		
		while(true) {
			System.out.print("Coeficiente: ");
			int coeff = sc.nextInt();
			System.out.println("Exponente: ");
			int exp = sc.nextInt();
			
			if(exp == 0) {
				break;
			} else {
				lista.add(coeff, exp);
			}
		}
		
		System.out.print("Introduzca valor de x: ");
		int x = sc.nextInt();
		lista.calculate(x);
		System.out.println(lista.sumSolutions());
		
	}
}
