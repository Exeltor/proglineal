
public class List {
	
	Node head, tail;
	
	public List() {
		head = null;
		tail = null;
	}
	
	public void add(int coefficient, int exponent) {
		head = new Node(coefficient, exponent, head);
		
		if(tail == null) {
			tail = head;
		}
	}
	
	public void calculate(int x) {
		for(Node aux = head; aux != null; aux = aux.next) {
			aux.solution = Math.pow(x, aux.exponent) * aux.coefficient;
		}
	}
	
	public double sumSolutions() {
		double sum = 0;
		for(Node aux = head; aux != null; aux = aux.next) {
			sum += aux.solution;
		}
		
		return sum;
	}
	
	

}
