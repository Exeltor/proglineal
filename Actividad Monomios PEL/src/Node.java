
public class Node {
	
	int coefficient;
	int exponent;
	double solution;
	Node next;
	
	public Node(int coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	public Node(int coefficient, int exponent, Node node) {
		this.coefficient = coefficient;
		this.exponent = exponent;
		this.next = node;
	}

}
