
public class Node {
	
	public int dato;
	public Node next;
	
	public Node(int dato) {
		this.dato = dato;
	}
	
	public Node(int dato, Node puntero) {
		this.dato = dato;
		next = puntero;
	}
	
	public int getDato() {
		return dato;
	}

}
