
public class Lista {
	
	protected Node header, trailer;
	int size = 0;
	
	public Lista() {
		header = null;
		trailer = null;
	}
	
	public void add(int valorDato) {
		header = new Node(valorDato, header);
		
		if(trailer == null) {
			trailer = header;
		}
		
		size++;
	}
	
	public boolean isEmpty() {
		return header == null;
	}
	
	public void deleteFirst() {
		if(!isEmpty()) {
			header = header.next;
		}
	}
	
	public void deleteLast() {
		if(!isEmpty()) {
			Node current = header;
			Node next = current.next;
			
			while(next.next != null) {
				current = next;
				next = next.next;
			}
			
			trailer = current;
			trailer.next = null;
		}
	}
	
	public void deleteElem(int element) {
		if(!isEmpty()) {
			Node current = header;
			Node next = current.next;
			
			while(next.dato != element) {
				current = next;
				next = next.next;
			}
			
		
			current.next = next.next;
		}
	}
	
	public void deleteIndex(int index) {
		Node aux1 = header;
		Node aux2 = header.next;
		
		if(!isEmpty()) {
			if(index <= size-1) {
				for(int pos = 0; pos < index-1; pos++) {
					aux1 = aux1.next;
					aux2 = aux2.next;
				}
				
				aux1.next = aux2.next;
			} else {
				System.out.println("no such index");
			}
		}
	}
	
	
	public String toString() {
		for(Node aux = header; aux != null; aux = aux.next) {
			System.out.print(aux.getDato() + " ");
		}
		
		return "";
	}

}
