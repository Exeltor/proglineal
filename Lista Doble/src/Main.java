/**
 * 
 * @author Alexey Zhelezov
 *
 */

public class Main {
	
	public static void main(String[] args) {
		/*
		 * Creo una lista vacia y defino un numero entero tries, inicializandolo a 0, para mantener un contador de las veces que he tenido que comprobar si los 
		 * nodos coinciden en la ruleta.
		 */
		
		DList lista = new DList();
		int tries = 0;
		
		//A�ado 7 nodos a la lista, con datos generados aleatoriamente, los cuales pueden ser un numero del 1 al 9. Para esto, utilizo el metodo addFirst
		for(int i = 0; i < 7; i++) {
			lista.addFirst((int)(Math.random()*9 + 1));
		}
		
		//Imprimo la lista sin modificar, para mostrar los nodos a�adidos.
		System.out.println(lista);
		
		/*
		 * Inicio un bucle infinito, en el cual genero 2 numeros aleatorios para luego ser introducidos en la ruleta. Con cada inicio del bucle, incremento
		 * el contador. Este contador sirve para indicar cuantas veces se ha tenido que reiniciar la ruleta para que los nodos coincidan. Cuando los nodos
		 * coinciden, el bucle se rompe.
		 * 
		 * Al coincidir, tambien se cambia el dato del nodo resultante a la cantidad de veces que se ha tenido que iniciar la ruleta hasta llegar en un
		 * nodo coincidente.
		 */
		while(true) {
			int ranNum1 = (int)(Math.random()*10 + 1);
			int ranNum2 = (int)(Math.random()*10 + 1);
			
			if(lista.nodesCoincide(ranNum1,  ranNum2)) {
				lista.node1.data = tries;
				break;
			}
			
			tries++;
		}
		
		
		/*
		 * Para finalizar, imprimo el numero de intentos que se han realizado hasta que los nodos coincidan e imprimo la lista modificada, donde muestro
		 * la lista modificada, para poder comprobar en que nodo ha coincidido la ruleta.
		 */
		System.out.println("Numero de intentos: ");
		System.out.print(lista.node1.data);
		System.out.println();
		System.out.println("------------------------");
		System.out.println(lista);
	}

}
