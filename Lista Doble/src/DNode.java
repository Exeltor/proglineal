/**
 * 
 * @author Alexey Zhelezov
 *
 */

public class DNode {
	
	/**
	 * Inicializo los atributos de un nodo. El int data se utiliza para guardar un numero entero en el todo.
	 * Los nodos prev y next se utilizan como punteros al nodo anterior y al siguiente, respectivamente.
	 */
	
	int data;
	DNode prev, next;
	
	/**
	 * Este constructor simple se utiliza para crear un nodo sin punteros, solo introduciendo el dato a guardar en el nodo.
	 * Este constructor no se utiliza en este programa pero se podria utilizar para crear una lista de un solo elemento.
	 * 
	 * @param data
	 */
	
	public DNode(int data) {
		this.data = data;
	}
	
	/*
	 *Este constructor es el que utilizo en la adicion de nuevos nodos en la lista, donde indico el dato a guardar en el nodo, y tambien defino los
	 *punteros del nodo anterior y siguiente, para posteriormente poder referirme a ellos en la lista y poder navegar por la lista.
	 */
	
	public DNode(int data, DNode prev, DNode next) {
		this.data = data;
		this.prev = prev;
		this.next = next;
	}

}
