/**
 * 
 * @author Alexey Zhelezov
 *
 */

public class DList {
	
	//Inicializo todas las variables que utilizare para mantener la lista, como head y tail para indicar los nodos inicio y fin de la cola
	//Size se utiliza para tener guardado el tama�o de la cola a medida que se van a�adiendo elementos a esta.
	//node1 y node2 se utilizan para consultar los datos de los nodos coincidentes del metodo nodesCoincide de la actividad.
	
	DNode head, tail, node1, node2;
	int size, cont = 0;
	int headData, tailData;
	
	
	/**
	 * Este es el metodo constructor de la lista, donde se inicializan los punteros principales
	 * cabeza y cola de la lista, a null, para asi crear una lista vacia.
	 */
	public DList() {
		head = null;
		tail = null;
	}
	
	/**
	 * Este metodo se utiliza para a�adir un nodo al principio de la lista, es decir, desde el puntero cabeza(en este cado head). 
	 * 
	 * @param data
	 */
	
	public void addFirst(int data) {
		/**
		 * asigno el nodo nuevamente creado a ser la cabeza de la cola, introduciendo todos los parametros requeridos para su creacion, es este caso
		 * el dato del nodo, el punteroAnterior que sera null, y el punteroSiguiente, que sera el nodo cabeza anterior.
		 */
		
		head = new DNode(data, null, head);
		
		
		/**
		 * Esta comprobacion se hace si se requiere entrelazar el nodo nuevamente creado con el nuevo que ha sido la cabeza anterior de la lista, en la
		 * direccion de atras. Esta linea ha sido a�adida ya que si todos los nodos hayan sido a�adidos por la cola, habra necesidad de entrelazarlos para
		 * atras.
		 */
		if(head.next != null){
			if(head.next.prev == null) {
				head.next.prev = head;
			}
		}
		
		/**
		 * Esto es la comprobacion si la lista esta vacia. Si lo esta, el nuevo nodo introducido sera la cabeza y la cola a la vez, ya que la lista solo
		 * tiene un nodo, y por tanto, el unico elemento en esta es el inicio y el fin de la lista a la vez.
		 */
		
		if(tail == null) {
			tail = head;
		}
		
		size++;
	}
	
	/**
	 * Este metodo es muy similar en funcionamiento a la adicion de nodos por el principio, simplemente en este caso, todos los datos introducidos
	 * y el entrelazamiento esta hecho al reves, para facilitar la adicion de nodos por la cola de la lista.
	 * 
	 * @param data
	 */
	
	public void addLast(int data) {
		tail = new DNode(data, tail, null);
		tail.prev.next = tail;
		
		if(tail.prev != null){
			if(tail.prev.next == null) {
				tail.prev.next = tail;
			}
		}
		
		if(head == null) {
			head = tail;
		}
		
		size++;
	}
	
	/**
	 * Este metodo es un metodo auxiliar al metodo nodesCoincide, el cual uso para navegar los nodos de la lista empezando por la cabeza de la misma.
	 * Este metodo se mueve una cantidad fija de pasos, que se define en el parametro "max" del metodo, y devuelve el nodo en el que se ha parado el 
	 * contador.
	 * 
	 * @param max
	 * @return
	 */
	
	public DNode moveForward(int max) {
		
		//Se inicia un loop infinito de rebote hasta llegar al numero maximo de pasos.

        while(true) {
        	//me muevo hacia delante por la lista hasta llegar al final de la lista, incrementando el contador.
        	
        	for(DNode aux = head; aux != null; aux = aux.next) {
                if(cont == max) {
                	//devuelvo el nodo resultante una vez llegado al maximo.
                	
                    return aux;
                }
                cont++;
            }
        	cont--;
        	
        	//me muevo hacia atras por la lista llegando hasta el principio de esta, incrementando el contador.
        	
        	for(DNode aux = tail; aux != null; aux = aux.prev) {
                if(cont == max) {
                	//devuelvo el nodo resultante una vez llegado al maximo.
                	
                    return aux;
                }
                cont++;
            }
        	
        	cont--;
        }
	}
	
	/**
	 * Este metodo funciona exactamente igual que el anterior, simplemente que se empieza por la cola de la lista.
	 * 
	 * @param max
	 * @return
	 */
	
	public DNode moveBackwards(int max) {

        while(true) {
        	for(DNode aux = tail; aux != null; aux = aux.prev) {
                if(cont == max) {
                    return aux;
                }
                cont++;
            }
        	
        	cont--;
        	
        	for(DNode aux = head; aux != null; aux = aux.next) {
                if(cont == max) {
                    return aux;
                }
                cont++;
            }
        	
        	cont--;
        }
	}
	
	/**
	 * Este metodo pide 2 numeros aleatorios, los cuales son introducidos a los respectivos metodos auxiliares como el numero de pasos por hacer.
	 * Si los nodos resultantes de cada funcion coinciden, se devuelve un "True".
	 * 
	 * @param rndmNumber1
	 * @param rndmNumber2
	 * @return
	 */
	
	public boolean nodesCoincide (int rndmNumber1, int rndmNumber2) {
		cont = 0;
		node1 = moveForward(rndmNumber1);
		cont = 0;
		node2 = moveBackwards(rndmNumber2);
		
		if(node1.data == node2.data) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Este metodo se utiliza para imprimir la lista en consola mediante un for loop.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	public String toString() {
		String ret = "";
		
		for(DNode aux = head; aux != null; aux = aux.next) {
			if(aux == head) {
				ret += "[" + aux.data + "]";
			} else {
				ret += " <----> [" + aux.data + "]";
			}
		}
		
		return ret;
	}

}
