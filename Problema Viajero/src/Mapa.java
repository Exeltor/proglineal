import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Mapa {
	
	ArrayList<Nodo> nodos = new ArrayList<>();
	ArrayList<Conexion> caminos = new ArrayList<>();
	ArrayList<String> permutaciones = new ArrayList<>();
	Map<String, Integer> puntuaciones = new HashMap<>();
	String permutate = "";
	
	private Nodo getNode(String label) {
		for(Nodo nodo : nodos) {
			if(nodo.getLabel().equals(label)) {
				return nodo;
			}
		}
		
		return null;
	}
	
	private Conexion getConnection(String startLabel, String endLabel) {
		for(Conexion conexion : caminos) {
			if(conexion.checkStartNodeLabel(startLabel) && conexion.checkEndNodeLabel(endLabel)) {
				return conexion;
			}
		}
		
		return null;
	}
	
	public void addNode(String label) {
		if(getNode(label) == null) {
			nodos.add(new Nodo(label));
		}
	}
	
	public void addConnection(String startLabel, String endLabel, int cost) {
		if(getConnection(startLabel, endLabel) == null) {
			caminos.add(new Conexion(getNode(startLabel), getNode(endLabel), cost));
		}
	}
	
	public void encontrarCamino(String start) {
		if(getNode(start) != null) {
			for(Nodo nodo : nodos) {
				if(!nodo.getLabel().equals(start)) {
					permutate += nodo.getLabel();
				}
			}
		} else {
			System.out.println("El nodo no existe");
		}
		
		permutaciones(permutate, 0, permutate.length()-1);
		
		for(String permutacion : permutaciones) {
			String finalCheck = start + permutacion + start;
			int puntuacion = 0;
			for(int i = 0; i < finalCheck.length()-1; i++) {
				int puntos = getConnection(String.valueOf(finalCheck.charAt(i)), String.valueOf(finalCheck.charAt(i+1))).getCost();
				puntuacion += puntos;
			}
			
			puntuaciones.put(finalCheck, puntuacion);			
		}
		
		mostrarResultados();
		
	}
	
	public void mostrarPermutaciones() {
		for(String permutacion : permutaciones) {
			System.out.println(permutacion);
		}
	}
	
	private void permutaciones(String str, int startInd, int endInd) {
		if(startInd == endInd) {
			permutaciones.add(str);
		} else {
			for(int i = startInd; i <= endInd; i++) {
				str = intercambio(str, startInd, i);
				permutaciones(str, startInd+1, endInd);
				str = intercambio(str, startInd, i);
			}
		}
	}
	
	private String intercambio(String str, int i, int j) {
		char temp;
		char[] charArray = str.toCharArray();
		temp = charArray[i];
		charArray[i] = charArray[j];
		charArray[j] = temp;
		return String.valueOf(charArray);
	}
	
	private void mostrarResultados() {
		for(String key : puntuaciones.keySet()) {
			System.out.println("Camino: " + key + ", Puntuacion: " + puntuaciones.get(key));
		}
	}
	
	public String toString() {
		System.out.println("Nodos");
		System.out.println("----------------------");
		for(Nodo nodo : nodos) {
			System.out.println(nodo.getLabel());
		}
		
		System.out.println("----------------------");
		System.out.println("Conexiones");
		for(Conexion conexion : caminos) {
			System.out.println(conexion.getStart() + " <-------> " + conexion.getEnd() + ", Coste: " + conexion.getCost());
		}
		
		System.out.println("----------------------");
		
		return "";
	}

}
