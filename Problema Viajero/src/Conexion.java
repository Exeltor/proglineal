
public class Conexion {
	
	private Nodo start, end;
	private int cost;
	
	public Conexion(Nodo start, Nodo end, int cost) {
		this.start = start;
		this.end = end;
		this.cost = cost;
	}
	
	public boolean checkStartNodeLabel(String label) {
		if(this.start.getLabel().equals(label) || this.end.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public boolean checkEndNodeLabel(String label) {
		if(this.end.getLabel().equals(label) || this.start.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public int getCost() {
		return cost;
	}
	
	public String getStart() {
		return start.getLabel();
	}
	
	public String getEnd() {
		return end.getLabel();
	}

}
