
public class main {
	
	public static void main(String[] args) {
		Mapa mapa = new Mapa();
		
		mapa.addNode("A");
		mapa.addNode("B");
		mapa.addNode("C");
		mapa.addNode("D");
		mapa.addConnection("A", "B", 5);
		mapa.addConnection("A", "D", 3);
		mapa.addConnection("A", "C", 8);
		mapa.addConnection("B", "C", 6);
		mapa.addConnection("B", "D", 7);
		mapa.addConnection("C", "D", 12);
		
		System.out.println(mapa);
		mapa.encontrarCamino("A");
	}

}
